<?php

// a set of recommended filters

$filters->addDOMFilter('Metadata');
$filters->addDOMFilter('News');
$filters->addDOMFilter('NewsLinker');
$filters->addDOMFilter('NewsDate');
$filters->addDOMFilter('GenerateTableOfContents');
$filters->addDOMFilter('Acronymizer');
$filters->addDOMFilter('Quoter');
$filters->addDOMFilter('RSSFromGit');
$filters->addDOMFilter('RSSGenerator');
$filters->addDOMFilter('AbsolutePath');
$filters->addDOMFilter('IEConditionalComments');
$filters->addDOMFilter('MarkLeadParagraphs');
$filters->addDOMFilter('AutoStyle');
$filters->addDOMFilter('ConfigLinker');

$filters->addPostTextFilter('Fragment');

require_once 'external/spyc/spyc.php';
require_once 'external/php-markdown/markdown.php';
$filters->addMarkup('text', 'Markdown');
