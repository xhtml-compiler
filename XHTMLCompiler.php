<?php

/**
 * Super singleton registry that manages application context.
 */
class XHTMLCompiler
{

    // SINGLETON FUNCTIONALITY

    /** Private instance of singleton */
    private static $_instance;

    /** Private constructor, prevents other people from making it */
    private function __construct() {
        $this->loadConf();
    }

    /** Retrieves the single instance of the object */
    static public function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Overloads the instance with another one, usually a mock object
     * @param Object substitute for XHTMLCompiler
     */
    static public function setInstance($stub) {
        self::$_instance = $stub;
    }

    // REGISTRY FUNCTIONALITY

    /** Private instance of PHP wrapper */
    private static $_PHPWrapperInstance;

    /** Retrieves the single instance of the PHP wrapper */
    static public function getPHPWrapper() {
        if(is_null(self::$_PHPWrapperInstance)) {
            self::$_PHPWrapperInstance = new XHTMLCompiler_PHP();
        }
        return self::$_PHPWrapperInstance;
    }

    /**
     * Overloads the instance with another one, usually a mock object
     * @param Object substitute for XHTMLCompiler
     */
    static public function setPHPWrapper($stub) {
        self::$_PHPWrapperInstance = $stub;
    }

    // PLUGIN/CONFIGURATION FUNCTIONALITY

    protected $configKeys = array('allowed_dirs', 'directory_index',
        'indexed_dirs', 'web_path', 'web_domain', 'viewvc_url',
        'debug', 'error_xsl', 'admin_email', 'from_email',
        'error_log', 'error_mute', 'configdoc', 'smtp_transport', 'dsn');
    protected $config = array();
    protected $filterManager;

    public function loadConf() {
        $filters = new XHTMLCompiler_FilterManager();
        require dirname(__FILE__) . '/config.default.php'; // defaults

        // user configuration
        if (file_exists($f = dirname(__FILE__) . '/conf/config.php')) require $f;
        if (file_exists($f = dirname(__FILE__) . '/config.php')) require $f;

        $this->config = compact($this->configKeys);
        $this->filterManager = $filters;
    }
    public function getConf($key) {
        if (!isset($this->config[$key])) throw new Exception('No such configuration keypair ' . $key);
        return $this->config[$key];
    }
    public function getFilterManager() {return $this->filterManager;}

}
