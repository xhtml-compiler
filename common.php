<?php

if (!version_compare(PHP_VERSION, "5", ">=")) exit('Requires PHP 5.');

// setup our error reporting, ideally this would be in php.ini but shared
// hosting environment requires a little more flexibility.
error_reporting(E_STRICT | E_ALL);

define('XHTMLCOMPILER', dirname(__FILE__));

require_once 'functions.php';

require_once 'XHTMLCompiler.php';

require_once 'XHTMLCompiler/PHP.php';
require_once 'XHTMLCompiler/Exception.php';

require_once 'XHTMLCompiler/Page.php';
require_once 'XHTMLCompiler/File.php';
require_once 'XHTMLCompiler/Directory.php';

require_once 'XHTMLCompiler/RSSFeed.php';

require_once 'XHTMLCompiler/Filter.php';
require_once 'XHTMLCompiler/FilterManager.php';
require_once 'XHTMLCompiler/TextFilter.php';
require_once 'XHTMLCompiler/DOMFilter.php';
require_once 'XHTMLCompiler/DOMFilter/NewsBase.php';

// needed for error reporting
require_once 'external/swiftmailer/lib/swift_required.php';

set_exception_handler('xhtmlcompiler_exception_handler');
$xc = XHTMLCompiler::getInstance(); // invoke the super-object

check_errors();
register_shutdown_function('check_errors');

ini_set('display_errors', false);
ini_set('log_errors', true);
ini_set('error_log', dirname(__FILE__) . '/errors.log');

if($xc->getConf('debug')) {
    restore_error_handler();
    ini_set('display_errors', true);
}

// set working directory to parent directory (slightly hacky)
// if we want this to be able to be included in other apps,
// this must be removed
chdir( dirname(__FILE__) . '/..' );

require_once 'external/phpgit/library/Git.php';
require_once 'external/htmlpurifier/library/HTMLPurifier.auto.php';
require_once 'external/htmlpurifier/extras/HTMLPurifierExtras.auto.php';

// load Doctrine
require_once 'external/doctrine/lib/Doctrine.php';
spl_autoload_register(array('Doctrine', 'autoload'));

Doctrine::loadModels(dirname(__FILE__) . '/models/generated');
Doctrine::loadModels(dirname(__FILE__) . '/models');

Doctrine_Manager::connection($xc->getConf('dsn'));

