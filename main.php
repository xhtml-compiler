<?php

/**
 * Takes a cache miss and generates the file if it is valid.
 * @note Use ?purge=1 in order to force regeneration of the file
 */

require 'common.php';

$xc = XHTMLCompiler::getInstance();

$page = get_page_from_get();
if ($page === false) $page = get_page_from_server();
$page = normalize_index($page, $xc->getConf('directory_index'));

$page = new XHTMLCompiler_Page($page);
$page->display();
