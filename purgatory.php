<?php

$cookie = 'purgatory';
$mode = isset($_COOKIE[$cookie]) ? (bool) $_COOKIE[$cookie] : false;

// purgatory mode lasts half a year
if (isset($_POST['purgatory'])) {
    $mode = !$mode;
    setcookie($cookie, $mode ? '1' : '0',
        time() + 60 * 60 * 24 * 7 * 21, '/'
    );
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Purgatory-mode - XHTML Compiler</title>
    <style type="text/css">
        form {text-align: center;}
        fieldset {border: 0;}
    </style>
</head>
<body>
    <h1>Purgatory-mode</h1>
    <p>Purgatory-mode is a special cookie that causes all requests to
    redirect to <code>main.php</code>. As such, it's good for page
    developers who want to see their changes in real-time without
    having to manually append <code>?purge=1</code> to their page name.</p>
    <p>Note that purgatory-mode will still check if the source file
    is newer than the cache file. To override completely, you must use
    the purge parameter.</p>
    <form method="post" action="">
        <fieldset>
            <p>Purgatory mode is: <strong style="color:<?php echo $mode ? '#F00' : '#090' ?>"><?php echo $mode ? 'On' : 'Off' ?></strong></p>
            <input name="purgatory" type="submit" value="Toggle purgatory-mode" />
        </fieldset>
    </form>
</body>
</html>
