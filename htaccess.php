<?php

/**
 * Generates the appropriate mod_rewrite rules in the htaccess file.
 * @note If a .htaccess.in prototype file is present in the directory,
 *       it will be used as the "base" htaccess to determine the new
 *       one, and local changes to .htaccess will ALWAYS be overwritten
 * @todo Create an HTAccess object to edit
 */

require 'common.php';

$xc = XHTMLCompiler::getInstance();

$identifier_begin = '# BEGIN xhtml-compiler/main.php mod_rewrite';
$identifier_end   = '# END xhtml-compiler/main.php mod_rewrite';
$identifier_here  = '# HERE xhtml-compiler/main.php mod_rewrite';

if (file_exists('.htaccess')) {

    // do time check
    $files_to_check = array(
        XHTMLCOMPILER . '/config.php',
        XHTMLCOMPILER . '/config.default.php',
        XHTMLCOMPILER . '/config.smoketest.php',
        XHTMLCOMPILER . '/conf/config.php',
        XHTMLCOMPILER . '/htaccess.php',
        'redirects.txt',
        '.htaccess.in',
    );

    $mtime_htaccess = filemtime('.htaccess');

    $no_changes_needed = true;
    foreach ($files_to_check as $file) {
        if (file_exists($file) && filemtime($file) > $mtime_htaccess) {
            $no_changes_needed = false;
            break;
        }
    }

    if ($no_changes_needed) {
        throw new XHTMLCompiler_Exception(503, 'Nop',
            'No changes detected in relevant files');
    }

    if (!file_exists('.htaccess.in')) {
        $contents = file_get_contents('.htaccess');
    } else {
        $contents = file_get_contents('.htaccess.in');
    }

    // do writeability check
    if (
        (
            strpos($contents, $identifier_begin) === false ||
            strpos($contents, $identifier_end) === false
        ) &&
        strpos($contents, $identifier_here) === false
    ) {
        throw new XHTMLCompiler_Exception(503, false,
            'Pre-existing htaccess not configured to accept new rules');
    }

    // replace old rules with new set
    $regex =
        '/' .
            preg_quote($identifier_begin, '/') .
            '.+?' .
            preg_quote($identifier_end, '/') .
        '/s';

    $contents = preg_replace($regex, $identifier_here, $contents);

} else {
    $contents = $identifier_here;
}

// build the new htaccess
$n = array();
$n[] = $identifier_begin;
$n[] = 'Options -Multiviews';
$n[] = 'RewriteEngine on';
$n[] = 'RewriteBase ' . $xc->getConf('web_path') . '/';

// create permanent redirects
if (file_exists('redirects.txt')) {
    $redirects = explode("\n", file_get_contents('redirects.txt'));
    foreach ($redirects as $redirect) {
        $redirect = trim($redirect) . ' ';
        if ($redirect === ' ') continue;
        if ($redirect[0] === '#') continue;
        list($src, $dest, $p) = explode(' ', $redirect);
        if ($p !== '[P]') $src = '^' . preg_quote($src) . '$';
        // We use rewrite to prevent the appending of ?f= querystring
        $n[] = 'RewriteRule ' . $src . ' ' . $dest . ' [R=permanent,L]';
    }
}

$big_exp = array();
$directory_index = $xc->getConf('directory_index');
$indexed_dirs = $xc->getConf('indexed_dirs');
$allowed_dirs = $xc->getConf('allowed_dirs');
foreach ($allowed_dirs as $dir => $recursive) {
    $r = '';
    if ($recursive) {
        $r = "([^/]+/)*"; // escaped slashes not necessary
    }
    $len = strlen($dir);
    $slash = (!$len || $dir[$len-1] === '/') ? '' : '/';
    $dir_exp = preg_quote($dir) . $slash . $r;
    $big_exp[$dir] = $dir_exp;
}
$full_dir_exp = implode('|', $big_exp);

// prefer the extension-less URL
$n[] = 'RewriteCond %{THE_REQUEST} \.html\x20';
$n[] = "RewriteRule ^(($full_dir_exp)[^/]+)\.html$ \$1 [NS,R=301]";

foreach ($allowed_dirs as $dir => $recursive) {
    if (is_array($indexed_dirs)) {
        $intercept = isset($indexed_dirs[$dir]) ? $indexed_dirs[$dir] : true;
    } else {
        $intercept = $indexed_dirs;
    }
    if (is_string($directory_index) && $intercept) {
        // setup index rewrite
        $n[] = "RewriteRule ^({$big_exp[$dir]})$ \$1$directory_index";
    }
}

// allow pretty extension-less URLs for HTML pages
// this could be generalized for other URLs
$n[] = 'RewriteCond %{REQUEST_FILENAME} !-f';
$n[] = 'RewriteCond %{REQUEST_FILENAME}.html -f';
$n[] = 'RewriteCond %{HTTP_ACCEPT} text/html';
$n[] = "RewriteRule ^(($full_dir_exp)[^/]+)$ \$1.html [N]";

// basic redirection if it doesn't exist
$n[] = 'RewriteCond %{REQUEST_FILENAME} !-f [OR]';
$n[] = 'RewriteCond %{QUERY_STRING} purge=1 [OR]';
$n[] = 'RewriteCond %{HTTP_COOKIE} purgatory=1';
$n[] = "RewriteRule ^(($full_dir_exp)[^/]+\.html)$ xhtml-compiler/main.php?f=\$1 [L,QSA]";

// if purge is set, also handle directories
$n[] = 'RewriteCond %{QUERY_STRING} purge=1';
$n[] = "RewriteRule ^($full_dir_exp)$ xhtml-compiler/main.php?f=\$1 [L,QSA]";

// add application/xhtml+xml if the browser supports it
$n[] = 'RewriteCond %{HTTP_ACCEPT} application/xhtml\\+xml';
$n[] = "RewriteRule ^(($full_dir_exp)[^/]+\.html)$ - \"[T=application/xhtml+xml,L]\"";

// xc-deps are forbidden to outside world
$n[] = '<Files ~ "\.xc-deps$">';
$n[] = '  Order allow,deny';
$n[] = '  Deny from all';
$n[] = '</Files>';

// errors.log is forbidden to outside world. In theory, this will occur only
// in xhtml-compiler/, but it won't hurt to deny it everywhere.
$n[] = '<Files errors.log>';
$n[] = '  Order allow,deny';
$n[] = '  Deny from all';
$n[] = '</Files>';

// setup RSS
$n[] = 'AddType application/rss+xml rss';
$n[] = 'AddCharset UTF-8 .rss';
$n[] = '<IfModule mod_headers.c>';
$n[] = '  <Files ~ "\.rss$">';
$n[] = '    Header append Cache-Control "no-cache, must-revalidate"';
$n[] = '  </Files>';
$n[] = '</IfModule>';

// set UTF-8 for HTML pages
$n[] = 'AddCharset UTF-8 .html';

$n[] = $identifier_end;

$contents = str_replace($identifier_here, implode($n, PHP_EOL), $contents);

file_put_contents('.htaccess', $contents);
chmod('.htaccess', 0644);

if (is_cli()) {
    echo "Okay: New .htaccess file successfully written\n";
    exit;
}

?><h1>200: Okay</h1>New <tt>.htaccess</tt> file successfully written
