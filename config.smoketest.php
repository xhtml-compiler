<?php

// this configuration file will load up necessary configuration
// values for running the smoketests in xhtml-compiler/smoketests
// activate using "require 'config.smoketest.php';"

$allowed_dirs['xhtml-compiler/smoketests'] = 0;
$allowed_dirs['xhtml-compiler/smoketests/recursive'] = 1;
$allowed_dirs['xhtml-compiler/smoketests/(special)'] = 0;
$allowed_dirs['xhtml-compiler/smoketests/DOMFilter'] = 1;
$allowed_dirs['xhtml-compiler/smoketests/Markup']    = 1;

$indexed_dirs['xhtml-compiler/smoketests/DOMFilter'] = false;

// if it's already been included, don't worry about it
// we may want to be a little bit nicer about redefinitions,
// and config.filters.php may not contain "all" filters
require_once 'config.filters.php';
