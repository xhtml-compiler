<?xml-stylesheet type="text/xml" href="#style1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xsl:version="1.0"
      xml:lang="en">
  <head>
    <title>Error: <xsl:value-of select="error/title"/></title>
    <!-- This code is slightly problematic if we install xhtml-compiler somewhere else -->
    <link rel="stylesheet" href="{/error/base}/xhtml-compiler/error.css" type="text/css" />
  </head>
  <body>
    <h1>Error: <xsl:value-of select="error/title"/></h1>
    <xsl:if test="error/details">
      <div class="details">
        <xsl:copy-of select="error/details/node()" />
      </div>
    </xsl:if>
    <xsl:if test="error/debug">
      <div class="debug">
        <div class="title">
          <h2>Debug</h2>
        </div>
        <div class="content">
          <p>Base directory is: <strong><xsl:value-of select="error/debug/base-dir"/></strong>.</p>
          <p>
            Exception thrown from <strong><xsl:value-of select="error/debug/file"/></strong>
            on line <strong><xsl:value-of select="error/debug/line"/></strong>.
          </p>
        </div>
      </div>
    </xsl:if>
  </body>
</html>
