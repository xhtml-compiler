<?php

require_once 'common.php';

if (!is_cli()) throw new XHTMLCompiler_Exception(403, "Cannot perform database generation from web");

chdir(XHTMLCOMPILER);

Doctrine::dropDatabases();
Doctrine::createDatabases();
Doctrine::generateModelsFromYaml('schema.yml', 'models');
Doctrine::createTablesFromModels('models');

