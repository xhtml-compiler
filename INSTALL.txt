Installation is simple: stick xhtml-compiler as a subdirectory of the website
you would like to build, and then create a config.php file in xhtml-compiler
(consult config.default.php for available configuration directives on how
to set them up; if you want some decent defaults you should include
config.filters.php).
