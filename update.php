<?php

/**
 * Updates all files within our jurisdiction.
 * @note This wants to be in a post-commit hook!
 */

require 'common.php';

$xc = XHTMLCompiler::getInstance();

// if in command line mode, allow more options
$type = 'normal';
if ($cli = (!ini_get('register_globals') && !empty($argv))) {
    set_time_limit(0);
    $type = isset($argv[1]) ? $argv[1] : 'normal'; // see below
} else {
    header('Content-type: text/plain');
}

// display a help file
if ($type == 'help') {
    echo 
'php update.php [type] -- Updates compiled HTML files.
[type] is:
  . normal : update existing files, remove orphans (default)
  . all : normal + create files without cached output
  . force : regenerate all files
  . clean : remove all cache and dependency files
';
    exit;
}

// clean up the cache
if ($type == 'clean') {
    
    $allowed_dirs = $xc->getConf('allowed_dirs');
    foreach ($allowed_dirs as $dir => $recursive) {
        $dir = new XHTMLCompiler_Directory($dir);
        
        // cleanup HTML cache files
        $files = $dir->scan('.html', $recursive);
        foreach ($files as $file) {
            if (!is_created_by_us($file)) continue;
            unlink($file);
        }
        
        // cleanup dependency files
        $files = $dir->scan('.xc-deps', $recursive);
        foreach ($files as $file) unlink($file);
        
        // cleanup fragment files
        $files = $dir->scan('.frag', $recursive);
        foreach ($files as $file) unlink($file);
        
        // cleanup RSS files (probably should be somewhere else)
        $files = $dir->scan('.rss', $recursive);
        foreach ($files as $file) {
            $contents = file_get_contents($file);
            if (strpos($contents, '<generator>XHTML Compiler</generator>') !== false) {
                unlink($file);
            }
        }
        
    }
    
    echo 'Removed all cache and dependency files.';
    exit;
}

// status arrays
$updated = array();
$created = array();
$orphans = array();

$allowed_dirs = $xc->getConf('allowed_dirs');
$markup = $xc->getFilterManager()->getMarkup();
do {
    $touched = false;
    foreach ($allowed_dirs as $dir => $recursive) {
        $dir = new XHTMLCompiler_Directory($dir);
        // search for source files
        foreach ($markup as $ext => $x) {
            $files = $dir->scan('.' . $ext, $recursive);
            foreach ($files as $file) {
                try {
                    $page = new XHTMLCompiler_Page($file);
                    if ($type == 'all' && !$page->isCacheExistent()) {
                        // generate a new page
                        $touched = true;
                        $page->generate();
                        $created[] = $page->getCachePath();
                    } elseif ($type == 'force') {
                        $page->generate();
                        $updated[] = $page->getCachePath();
                    }
                } catch (Exception $e) {
                    echo "Failed on $file\n";
                    echo $e . "\n";
                }
            }
        }
        // search for cache files
        $files = $dir->scan('.html', $recursive);
        foreach ($files as $file) {
            try {
                $page = new XHTMLCompiler_Page($file, true);
                // we're only updating files, not creating new ones
                if ($page->isSourceExistent()) {
                    if (
                        // don't do it for force, since it's already been done
                        $type != 'force' &&
                        $page->isCacheExistent() &&
                        $page->isCacheStale()
                    ) {
                        $page->generate();
                        $updated[] = $page->getCachePath();
                        $touched = true;
                    }
                } elseif (is_created_by_us($orphan = $page->getCachePath())) {
                    unlink($orphan); // orphan
                    $orphans[] = $orphan;
                }
            } catch (Exception $e) {
                echo "Failed on $file\n";
                echo $e . "\n";
            }
        }
    }
} while($touched && $type != 'force');

// nice error message
if (empty($updated)) $updated[] = '(none)';
echo wordwrap('Updated pages: ' . implode(', ', $updated)) . PHP_EOL;

if (empty($orphans)) $orphans[] = '(none)';
echo wordwrap('Removed orphans: ' . implode(', ', $orphans)) . PHP_EOL;

if ($cli && ($type != 'force')) {
    if (empty($created)) $created[] = '(none)';
    echo wordwrap('Created pages: ' . implode(', ', $created)) . PHP_EOL;
}
