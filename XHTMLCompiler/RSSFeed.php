<?php

/**
 * Representation of an RSS feed, convenience wrapper for quick building
 */
class XHTMLCompiler_RSSFeed
{

    /** Internal DOM that represents the feed */
    protected $doc;

    /** Convenient reference to channel node in $this->doc */
    protected $channel;

    /**
     * @param $title Title of the feed
     * @param $path Fully-formed webpath to feed
     * @param $description Optional description of feed
     * @param $lang Optional language of feed
     */
    public function __construct($title, $path, $description = null, $lang = null) {
        $this->doc = new DOMDocument('1.0', 'UTF-8');
        $this->doc->formatOutput = true;

        $rss = $this->doc->createElement('rss');
        $rss->setAttribute('version', '2.0');
        $this->doc->appendChild($rss);

        $channel = $this->doc->createElement('channel');
        $rss->appendChild($channel);

        $channel->appendChild($this->doc->createElement('title', $title));
        $channel->appendChild($this->doc->createElement('link', $path));
        if ($lang) {
            $channel->appendChild(
                $this->doc->createElement('language', $lang)
            );
        }
        if ($description) {
            $channel->appendChild(
                $this->doc->createElement('description', $description)
            );
        }
        $channel->appendChild(
            $this->doc->createElement('generator', 'XHTML Compiler')
        );

        $this->channel = $channel;
    }

    /**
     * Adds a news item to the RSS feed
     * @param $link Link to relevant article
     * @param $title Title of the news item
     * @param $date Date of the article
     * @param $body Short description of article
     */
    public function addItem($link, $title, $date, $body) {
        $item = $this->doc->createElement('item');
        $this->channel->appendChild($item);

        $body = preg_replace("/\s+/", ' ', $body);

        if ($title) $item->appendChild($this->doc->createElement('title', htmlspecialchars($title)));
        $item->appendChild($this->doc->createElement('pubDate', $date));
        $item->appendChild($this->doc->createElement('description', htmlspecialchars($body)));
        $item->appendChild($this->doc->createElement('link', htmlspecialchars($link)));
    }

    /**
     * Saves RSS feed to path
     * @param $path Path to save feed to
     */
    public function save($path) {
        $this->doc->save($path);
        chmod($path, 0644);
    }

}
