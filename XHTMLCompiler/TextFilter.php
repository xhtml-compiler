<?php

/**
 * Represents a filter that performs processing on documents. Read-only.
 */
abstract class XHTMLCompiler_TextFilter extends XHTMLCompiler_Filter
{

    /**
     * Defines a filter that processes string text.
     * @param $text String text to process
     * @param $page XHTMLCompiler_Page object representing the context
     * @param $manager Currently running XHTMLCompiler_FilterManager
     * @return Processed string text
     * @note These filters should be used sparingly, only when a DOM
     *       solution would not work.
     */
    abstract public function process($text, $page, $manager);

}
