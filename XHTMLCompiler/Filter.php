<?php

/**
 * Represents a filter that performs processing on documents. Read-only.
 */
abstract class XHTMLCompiler_Filter
{
    
    /**
     * Unique, human-readable identifier for the filter
     */
    protected $name;
    
    /** Returns the name of the filter */
    public function getName() {return $this->name;}
    
    /**
     * @param $name Overloading name. If you have to filters that have
     *        the same name, you can simple override the name of one of
     *        them to let them peacefully coexist.
     */
    public function __construct($name = false) {
        if (empty($this->name)) throw new Exception('Filter '.__CLASS__ .' must have a name');
        if ($name) $this->name = $name;
    }
    
}
