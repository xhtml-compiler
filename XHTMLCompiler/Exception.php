<?php

/**
 * Generic exception that is not expected to be recoverable.
 * @todo Internationalization
 */
class XHTMLCompiler_Exception extends Exception
{
    /**
     * In-depth HTML message on the nature of the error.
     */
    protected $details;

    /**
     * @param $code HTTP status code you wish to return
     * @param $message Brief title of the error
     * @param $details Details on the error (can be HTML)
     */
    public function __construct($code = 500, $message = false, $details = false) {
        parent::__construct($message, $code);
        $this->details = $details;
    }

    /** Returns details of exception */
    public function getDetails() {return $this->details;}

    public function __toString() {
        return $this->getCode() . ': ' . $this->getMessage() . "\n" .
            $this->getDetails() . "\n\n" . parent::__toString() . "\n";
    }


}
