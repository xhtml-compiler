<?php

/**
 * Represents a filter that performs processing on documents. Read-only.
 */
abstract class XHTMLCompiler_DOMFilter extends XHTMLCompiler_Filter
{

    /**
     * If set, DOMFilter will allocate a namespace for the filter
     * and assign it to this prefix.
     * @warning May not be 'html' or 'xc', because these are already
     *          allocated for the core namespaces.
     */
    protected $prefix;

    /**
     * Allocated namespace for the filter, or default 'xc' one.
     */
    protected $ns;

    /**
     * Lookup of namespace shortcuts to full namespace names.
     */
    protected $nsLookup;

    /**
     * List of attributes the extension adds to the generic xc namespace,
     * helps prevent naming conflicts.
     */
    protected $xcAttr = array();
    public function getXCAttributesDefined() {
        return $this->xcAttr;
    }


    /**
     * Defines a filter that processes a DOMDocument.
     * @note Return is not used as objects are passed "by reference", and
     *       thus edits we make will be globally reflected.
     * @param $dom DOMDocument to process
     * @param $page XHTMLCompiler_Page object representing the context
     * @param $manager Currently running XHTMLCompiler_FilterManager
     */
    abstract public function process(DOMDocument $dom, $page, $manager);

    /**
     * Performs common initialization of DOM and XPath
     * @note This must be called before you can use any of the convenience
     *       functions.
     */
    public function setup($dom) {
        $this->dom = $dom;
        $this->xpath = new DOMXPath($dom);
        // defaults
        $ns['html'] = "http://www.w3.org/1999/xhtml";
        $ns['xml']  = "http://www.w3.org/XML/1998/namespace";
        $this->ns = $ns['xc'] = "urn:xhtml-compiler";
        if (isset($this->prefix)) {
            if ($this->prefix == 'html' || $this->prefix == 'xc') {
                throw new Exception('Prefix for ' . get_class($this) .
                    'may not be the reserved ' . $this->prefix);
            }
            $this->ns = $ns[$this->prefix] = "urn:xhtml-compiler:" . $this->name;
        }
        foreach ($ns as $prefix => $uri) {
            $this->xpath->registerNamespace($prefix, $uri);
        }
        $this->nsLookup = $ns;
    }

    /**
     * XPath object for the current DOM (private: use query() to use it)
     * @todo Implement all member functions for this
     */
    private $xpath;

    /**
     * Current DOMDocument (private: use the instance passed to you via parameter)
     */
    private $dom;

    /**
     * Querys a DOM with an XPath expression
     * @param $expr XPath expression to evaluate
     * @param $context Context node
     */
    protected function query($expr, $context = false) {
        if (!$this->dom) throw new Exception('Filter must be setup before using convenience functions');
        if (!$context) return $this->xpath->query($expr);
        return $this->xpath->query($expr, $context);
    }

    /**
     * Retrieves a namespaced attribute from an element, and then deletes it.
     * @note This is best for proprietary attributes that, once you grab
     *       their data, they should be removed from the DOM for validation's
     *       sake.
     * @param $node Node to remove attribute from
     * @param $ns Namespace of attribute
     * @param $name Name of attribute
     * or
     * @param $node Node to remove attribute from
     * @param $name Name of attribute
     */
    protected function confiscateAttr($node, $ns_or_name, $name = null) {
        if (is_null($name)) {
            $name = $ns_or_name;
            $value = $node->getAttribute($name);
            $node->removeAttribute($name);
        } else {
            $ns = $ns_or_name;
            if (isset($this->nsLookup[$ns])) $ns = $this->nsLookup[$ns];
            $value = $node->getAttributeNS($ns, $name);
            $node->removeAttributeNS($ns, $name);
        }
        return $value;
    }
}
