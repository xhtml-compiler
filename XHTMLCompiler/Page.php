<?php

/**
 * Represents a page in our content management system. This is loosely
 * bound to the filesystem, although it doesn't actually refer to a
 * specific file, just a class of files.
 */
class XHTMLCompiler_Page
{

    /**
     * Filename identifier of this page without extension
     */
    protected $pathStem;

    /**
     * File extension of the source file
     * @note Previously, this parameter could only be xhtml, but since we've
     *       to be a little more flexible the 'markup' configuration variable
     *       will be used to figure out what extensions to look at, and then
     *       this parameter will be populated with the appropriate extension.
     */
    protected $sourceExt;
    /** File extension of cache/served files */
    protected $cacheExt = 'html';
    /** File extension of dependency files */
    protected $depsExt = 'xc-deps';

    /** Instance of XHTMLCompiler_File for source file */
    protected $source;
    /** Instance of XHTMLCompiler_File for cache file */
    protected $cache;
    /** Instance of XHTMLCompiler_File for dependency file */
    protected $deps;

    /** Instance of XHTMLCompiler_Directory for all of the above files*/
    protected $dir;

    /** Array of attributes about this file. Currently used by News/NewsLinker */
    public $attr = array();

    /**
     * Constructs a page object, validates filename for correctness
     * @param $path String path filename, can be from untrusted source
     * @param $mute Whether or not to stop the class from complaining when
     *              the source file doesn't exist. This is a stopgap measure,
     *              please replace with better exception handling.
     * @todo Cleanup into subroutines
     * @todo Factor out allowed_directories realpath'ing to config class
     */
    public function __construct($path, $mute = false) {

        $xc  = XHTMLCompiler::getInstance();
        $php = XHTMLCompiler::getPHPWrapper();
        $markup = $xc->getFilterManager()->getMarkup();

        // test file extension
        $info = pathinfo($path);
        if (
            empty($info['extension']) || (
                !isset($markup[$info['extension']]) &&
                $info['extension'] !== $this->cacheExt
            )
        ) {
            throw new XHTMLCompiler_Exception(403, 'Forbidden extension',
              'File extension cannot be processed by XHTML Compiler, check
              for faulty <code>.htaccess</code> rules.');
        }

        // test for directory's existence and resolve to real path
        $dir = $info['dirname'];
        if ($dir == '.') $dir .= '/';
        $dir = $php->realpath($dir);
        if ($dir === false) {
            throw new XHTMLCompiler_Exception(404, 'Missing directory',
                'Requested directory cannot be found; check your file
                path and try again.' );
        }
        if ($dir[strlen($dir)-1] == '/') $dir = substr($dir, 0, -1);

        $dirObj = new XHTMLCompiler_Directory($dir);
        $ok = $dirObj->isAllowed();

        if (!$ok) throw new XHTMLCompiler_Exception(403, 'Forbidden directory',
          'Requested directory is forbidden to XHTML Compiler; try
          accessing it directly or check for faulty <code>.htaccess</code> rules.');

        // cannot use pathinfo, since PATHINFO_FILENAME is PHP 5.2.0
        $this->pathStem = substr($path, 0, strrpos($path, '.'));

        // setup the files
        foreach ($markup as $ext => $impl) {
            $this->source = new XHTMLCompiler_File($this->pathStem . '.' . $ext);
            $this->sourceExt = $ext;
            if ($this->source->exists()) break;
        }
        $this->cache  = new XHTMLCompiler_File($this->pathStem . '.' . $this->cacheExt);
        $this->deps   = new XHTMLCompiler_File($this->pathStem . '.' . $this->depsExt);

        $this->dir    = new XHTMLCompiler_Directory(dirname($this->pathStem));

        if (!$mute && !$this->source->exists()) {
            // Apache may have redirected to an ErrorDocument which got directed
            // via mod_rewrite to us, in that case, output the corresponding
            // status code.  Otherwise, we can give the regular 404.
            $code = $php->getRedirectStatus();
            if (!$code || $code == 200) $code = 404;
            throw new XHTMLCompiler_Exception($code, 'Page not found', 'Requested page not found; check the URL in your address bar.');
        }
    }

    // Note: Do not use this functions internally inside the class

    /** Returns path stem, full filename without file extension */
    public function getPathStem() { return $this->pathStem; }
    /** Returns relative path to cache */
    public function getCachePath() { return $this->cache->getName(); }
    /** Returns relative path to source */
    public function getSourcePath() { return $this->source->getName(); }
    /** Returns source extension, w/o leading period */
    public function getSourceExt() { return $this->sourceExt; }
    /** Returns XHTMLCompiler_Directory representation of directory */
    public function getDir() { return $this->dir; }
    /** Returns directory of the files without trailing slash */
    public function getDirName() { return $this->dir->getName(); }
    /** Returns directory of the files with trailing slash (unless there is none) */
    public function getDirSName() { return $this->dir->getSName(); }
    /** Returns how deep from the root the file is */
    public function getDepth() { return substr_count($this->getSourcePath(), '/'); }

    /** Normalizes a relative path as if it were from this page's directory */
    public function normalizePath($path) {
        return $this->getDirName() . '/' . $path;
    }

    /**
     * Returns a fully formed web path with web domain to the file. This path
     * is valid anywhere on the web.
     */
    public function getWebPath() {
        $xc = XHTMLCompiler::getInstance();
        $domain = $xc->getConf('web_domain');
        if (!$domain) {
            throw new Exception('Configuration value web_domain must be set for command line');
        }
        return 'http://' . $domain . $this->getAbsolutePath();
    }

    /**
     * Returns a fully formed absolute web path valid anywhere on the
     * current domain to the cached file.
     */
    public function getAbsolutePath() {
        $xc = XHTMLCompiler::getInstance();
        $name = $this->cache->getName();
        // a little icky
        if ($name[0] !== '/') $name = "/$name";
        if (strncmp($name, './', 2) === 0) $name = substr($name, 1);
        return $xc->getConf('web_path') . $name;
    }

    /** Returns contents of the cache/served file */
    public function getCache() { return $this->cache->get(); }
    /** Returns contents of the source file */
    public function getSource() { return $this->source->get(); }

    /** Reports whether or not cache file exists and is a file */
    public function isCacheExistent() { return $this->cache->exists(); }
    /** Reports whether or not source file exists and is a file */
    public function isSourceExistent() { return $this->source->exists(); }

    /** Removes the cache file, forcing this page to be re-updated as if
        it were newly added.*/
    public function purge() { return $this->cache->delete(); }

    /**
     * Reports whether or not the cache is stale by comparing the file
     * modification times between the source file and the cache file.
     * @warning You must not call this function until you've also called
     *          isCacheExistent().
     */
    public function isCacheStale() {
        if (!$this->cache->exists()) {
            throw new Exception('Cannot check for stale cache when cache
                does not exist, please call isCacheExistent and take
                appropriate action with the result');
        }
        if ($this->source->getMTime() > $this->cache->getMTime()) return true;
        // check dependencies
        if (!$this->deps->exists()) return true; // we need a dependency file!
        $deps = unserialize($this->deps->get());
        foreach ($deps as $filename => $time) {
            if ($time < filemtime($filename)) return true;
        }
        return false;
    }

    /**
     * Writes text to the cache file, overwriting any previous contents
     * and creating the cache file if it doesn't exist.
     * @param $contents String contents to write to cache
     */
    public function writeCache($contents) {$this->cache->write($contents);}

    /**
     * Attempts to display contents from the cache, otherwise returns false
     * @return True if successful, false if not.
     * @todo Purge check needs to be factored into XHTMLCompiler
     */
    public function tryCache() {
        if (
            !isset($_GET['purge']) &&
            $this->cache->exists() &&
            !$this->isCacheStale()
        ) {
            // cached version is fresh, serve it. This shouldn't happen normally
            set_response_code(200); // if we used ErrorDocument, override
            readfile($this->getCachePath());
            return true;
        }
        return false;
    }

    /**
     * Generates the final version of a page from the source file and writes
     * it to the cache.
     * @note This function needs to be extended greatly
     * @return Generated contents from source
     */
    public function generate() {
        $source = $this->source->get();
        $xc = XHTMLCompiler::getInstance();
        $filters = $xc->getFilterManager();
        $contents = $filters->process($source, $this);
        $deps = $filters->getDeps();
        if (empty($contents)) return ''; // don't write, probably an error
        $contents .= '<!-- generated by XHTML Compiler -->';
        $this->cache->write($contents);
        $this->cache->chmod(0664);
        $this->deps->write(serialize($deps));
        return $contents;
    }

    /**
     * Displays the page, either from cache or fresh regeneration.
     */
    public function display() {
        if($this->tryCache()) return;
        $ret = $this->generate();
        if ($ret) {
            if (stripos($_SERVER["HTTP_ACCEPT"], 'application/xhtml+xml') !== false) {
                header("Content-type: application/xhtml+xml");
            } else {
                header("Content-type: text/html");
            }
        }
        echo $ret;
    }

    /**
     * Retrieves the Git_Repo that represents this page.
     */
    public function getRepo() {
        return new Git_Repo($this->source->getDirectory());
    }

    /**
     * Retrieves the filename relative to the Git repository root.
     */
    public function getGitPath() {
        $repo = $this->getRepo();
        // This won't work with bare repositories
        return $name = str_replace(
            '\\', '/', // account for Windows
            substr(
                realpath($this->source->getName()), // $repo->path is full
                strlen(dirname($repo->path))+1 // chop off "repo" path (w/o .git) + leading slash
            )
        );
    }

    /**
     * Retrieves the log that represents this page.
     */
    public function getLog($kwargs = array()) {
        // This doesn't account for sub-repositories
        $repo = $this->getRepo();
        return $repo->log('master', array($this->getGitPath()), array_merge(array('follow' => true), $kwargs));
    }

    // this is metadata stuff that needs to be moved and cached

    /**
     * Retrieves the DateTime this page was created, according to Git's logs.
     * If no logs are present, use filectime(), which isn't totally accurate
     * but is the best information present.
     */
    public function getCreatedTime() {
        // As a backwards-compatibility measure, we allow the first meta tag
        // with the specific signature:
        //    <meta name="Date" contents="..."
        // to specify an ISO 8601 formatted date (or date compatible with
        // GNU strtotime; Metadata will convert it into ISO 8601 as per
        // the Dublin core specification).
        $source = $this->source->get();
        if (($p = strpos($source, '<meta name="Date" content="')) !== false) {
            $p += 27; // cursor is now after the quote
            // Grab the time
            $time = substr($source, $p, strpos($source, '"', $p) - $p);
            return new DateTime($time);
        }

        $repo = $this->getRepo();
        // This is extremely memory inefficient, but I can't figure out
        // how to get Git to limit the commits (-n) without undoing
        // --reverse.
        $log = $repo->log('master', array($this->getGitPath()), array(
            'reverse' => true,
        ));
        if (empty($log)) {
            $date = new DateTime('@' . $this->source->getCTime());
        } else {
            $date = $log[0]->authoredDate;
        }
        $this->setTimezone($date);
        return $date;
    }

    /**
     * Retrieves the DateTime this page was last updated, according to Git's logs,
     * otherwise according to filemtime.
     */
    public function getLastModifiedTime() {
        $repo = $this->getRepo();
        $log = $repo->log('master', array($this->getGitPath()), array(
            'n' => 1,
        ));
        if (empty($log)) {
            $date = new DateTime('@' . $this->source->getMTime());
        } else {
            $date = $log[0]->authoredDate;
        }
        $this->setTimezone($date);
        return $date;
    }

    /**
     * Touches the source file, meaning that any files that depend on this
     * file should be regenerated.  XHTML Compiler knows, however,
     * that it's not the first time the cache has been generated. This is
     * weaker than purge().
     */
    public function touch() {
        $this->source->touch();
    }

    /**
     * Sets our default timezone to a date object; especially useful if it
     * was initialized with an @ isgn.
     */
    private function setTimezone($date) {
        $date->setTimezone(new DateTimeZone(date_default_timezone_get()));
    }

}
