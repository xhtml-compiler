<?php

/**
 * Generates an RSS feed from a page's associated Git log.
 */
class XHTMLCompiler_DOMFilter_RSSFromGit extends XHTMLCompiler_DOMFilter
{
    protected $name = 'RSSFromGit';
    /** Number of log entries to display */
    protected $limit = 10;
    /** Description of changelog to place in feed title */
    protected $description = 'Git changelog for %s';
    /** Compact title to place in <link> and other places */
    protected $title = 'Log for %s';
    protected $xcAttr = array('rss-from-vc', 'rss-from-git', 'rss-from-svn');

    public function process(DOMDocument $dom, $page, $manager) {
        $xc = XHTMLCompiler::getInstance();
        // implement gitweb/repo.or.cz support

        $logs = $page->getLog();

        // see if we need to make the link ourself
        $trigger = $this->confiscateAttr($dom->documentElement, $this->ns, 'rss-from-vc');
        if (!$trigger) $trigger = $this->confiscateAttr($dom->documentElement, $this->ns, 'rss-from-git');
        if ($trigger == 'yes') {
            // this shows up twice because we have to perform all
            // confiscations before aborting
            if (!$logs) return;
            $link = $dom->createElement('link');
            $link->setAttribute('rel', 'alternate');
            $link->setAttribute('type', 'application/rss+xml');
            $link->setAttribute('title', str_replace('%s', $page->getCachePath(), $this->title));
            $link->setAttribute('href', $page->getPathStem() . '.rss');
            $head = $this->query('//html:head')->item(0);
            $head->appendChild($link);
        } else {
            // grab the document's links to RSS feeds
            // link must be marked with xc:rss-from-git (or
            // xc:rss-from-svn, for BC reasons)
            // only one allowed
            $link = $this->query('//html:link[@xc:rss-from-vc]')->item(0);
            if (!$link) return; // nothing to do

            $trigger = $this->confiscateAttr($link, $this->ns, 'rss-from-vc');
            if ($trigger != 'yes') return;
            if (!$logs) return;
        }
        $manager->addDependency(__FILE__);

        $path = $page->getWebPath();

        $rss = new XHTMLCompiler_RSSFeed(
            $link->getAttribute('title'),
            $path,
            str_replace('%s', $page->getCachePath(), $this->description),
            $dom->documentElement->getAttribute('xml:lang')
        );

        for ($i = 0; $i < $this->limit && isset($logs[$i]); $i++) {
            $commit = $logs[$i];

            // :TODO: link to gitweb commitdiff
            $item_link = $path . '#git_' . $commit->id;

            // generate short message (first line) for title\

            $rss->addItem(
                $item_link,
                htmlspecialchars($commit->message),
                $d = $commit->committedDate->format('r'),
                // :TODO: Extend phpgit so that we can retrieve full message
                htmlspecialchars($commit->message)
            );
        }

        $rss->save(
            $page->normalizePath(
                $link->getAttribute('href')
            )
        );


    }
}
