<?php

/**
 * Enables Internet Explorer style conditional comments to survive XML
 * parsing by implementing a custom xc:ie-condition attribute to attach
 * to elements.
 */
class XHTMLCompiler_DOMFilter_IEConditionalComments extends XHTMLCompiler_DOMFilter
{

    protected $name = 'IEConditionalComments';
    protected $xcAttr = array('ie-condition');

    public function process(DOMDocument $dom, $page, $manager) {

        $nodes = $this->query( "//*[@xc:ie-condition]" );

        if ($nodes) $manager->addDependency(__FILE__);
        foreach ($nodes as $node) {
            $condition = $this->confiscateAttr($node, $this->ns, 'ie-condition');
            if ($condition) {
                $sxml = simplexml_import_dom($node);
                $code = $sxml->asXml();
                $comment = "[if $condition]>$code<![endif]";
                $parent = $node->parentNode;
                $parent->replaceChild($dom->createComment($comment), $node);
            }
        }

    }

}
