<?php

/**
 * This filter, closely related with the News filter, generates links
 * between news entries.
 */
class XHTMLCompiler_DOMFilter_NewsLinker extends XHTMLCompiler_DOMFilter
{
    protected $name = 'NewsLinker';
    public function process(DOMDocument $dom, $page, $manager) {
        if (!isset($page->attr['news'])) return;
        $manager->addDependency(__FILE__);

        // calculate forward and backward links
        $fs = new FSTools();
        // we assume that one directory up is the years directory. An alternate
        // algorithm may be needed if we decide to allow further nesting.
        // This code needs to be factored out
        $result = $fs->globr(dirname($page->getDirname()), '*.xhtml');
        // This doesn't work if there is more than one entry in a day
        sort($result);
        $prev = $next = null;
        $found = false;
        foreach ($result as $i => $entry) {
            $base = basename($entry);
            if (strlen($base) < 4 || !ctype_digit(substr($base, 0, 4))) {
                continue;
            }
            if ($page->getSourcePath() == $entry) {
                $found = true;
            } elseif (!$found) {
                $prev = $entry;
            } else {
                $next = $entry;
                break;
            }
        }

        $content = $dom->getElementById('content');
        $body = $content->parentNode;
        $nav = $dom->createElement('div');
        $nav->setAttribute('id', 'news-navigation');

        if ($prev) {
            $prevPage = new XHTMLCompiler_Page($prev);
            if (!$page->isCacheExistent()) {
                // Force a new cache run
                $prevPage->touch();
            }
            $prevDiv = $dom->createElement('div');
            $prevDiv->setAttribute('class', 'prev');
            $a = $dom->createElement('a', 'Previous');
            $a->setAttribute('href', $prevPage->getAbsolutePath());
            $prevDiv->appendChild($a);
            $nav->appendchild($prevDiv);
        }

        $indexDiv = $dom->createElement('div');
        $indexDiv->setAttribute('class', 'index');
        $a = $dom->createElement('a', 'Index');
        $indexDiv->appendChild($a);
        //$nav->appendChild($indexDiv);

        if ($next) {
            $nextPage = new XHTMLCompiler_Page($next);
            $nextDiv = $dom->createElement('div');
            $nextDiv->setAttribute('class', 'next');
            $a = $dom->createElement('a', 'Next');
            $a->setAttribute('href', $nextPage->getAbsolutePath());
            $nextDiv->appendChild($a);
            $nav->appendChild($nextDiv);
        }

        $body->appendChild($nav);
    }
}
