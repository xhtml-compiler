<?php

/**
 * Based on a list of known acronyms, populates of the title attribute
 * of acronym and abbr elements in documents.
 * @note Title is a bit of legacy baggage.
 */
class XHTMLCompiler_DOMFilter_Acronymizer extends XHTMLCompiler_DOMFilter
{

    protected $name = 'Acronymizer';

    /**
     * Associative array of recognized abbreviations/initialisms
     * @note To be used when each letter should be spelled out, such
     *       as HTML.
     */
    protected $abbreviations = array(
        // markup languages and related technologies
        'SGML' => 'Standard Generalized Markup Language',
        'HTML' => 'HyperText Markup Language',
        'XHTML' => 'Extensible HyperText Markup Language',
        'XML' => 'Extensible Markup Language',
        'RSS' => 'Really Simple Syndication',
        'DTD' => 'Document Type Definition',
        'CSS' => 'Cascading Style Sheets',
        'HTTP' => 'HyperText Transfer Protocol',
        // programming/apis
        'PHP' => 'PHP: HyperText Preprocessor',
        'CMS' => 'Content Management System',
        'API' => 'Application Programming Interface',
        'SVN' => 'Subversion',
        'XSLT' => 'Extensible Stylesheet Language Transformations',
        'SQL' => 'Structured Query Language',
        // web-app security
        'XSS' => 'Cross-Site Scripting',
        // organizations/groups
        'W3C' => 'World Wide Web Consortium',
        'RFC' => 'Request for Comment',
        'PECL' => 'PHP Extension Community Library',
        // character encodings
        'UTF-8' => '8-bit Unicode Transformation Format',
        // other
        'INI' => 'Initialization',
        'CPU' => 'Central Processing Unit',
        'LGPL' => 'Lesser GNU Public License',
        'FTP' => 'File Transfer Protocol',
        'URI' => 'Uniform Resource Identifier',
    );

    /**
     * Array of recognized acronyms.
     * @note Acronyms can be spoken literally, if in doubt, make it
     *       an abbreviation.
     * @todo Make a public API for this, allow multiple acronym sets
     *       and different precedences for them.
     */
    protected $acronyms = array(
        // programming
        'SAX' => 'Simple API for XML',
        'DOM' => 'Document Object Module',
        'PEAR' => 'PHP Extension and Application Repository',
        'ASCII' => 'American Standard Code for Information Interchange',
        'SHA-1' => 'Secure Hash Algorithm',
        // paradigms
        'WYSIWYG' => 'What You See Is What You Get',
        'WYSIWYM' => 'What You See Is What You Mean',
    );

    public function process(DOMDocument $dom, $page, $manager) {
        $nodes = $this->query("//html:acronym[not(@title)]");
        foreach ($nodes as $node) $this->addAdvisoryTitle($node, $this->acronyms);

        $nodes = $this->query("//html:abbr[not(@title)]");
        foreach ($nodes as $node) $this->addAdvisoryTitle($node, $this->abbreviations);

        // add self as dependency; when acronym lists change, so does the page
        $manager->addDependency(__FILE__);
    }

    protected function addAdvisoryTitle($node, $lookup) {
        $key = $node->textContent;
        if (!isset($lookup[$key])) {
            // not fatal, but good to let the document author know
            trigger_error(htmlspecialchars($key) .
                ' is not a recognized acronym/abbreviation (missing title attribute)');
            return;
        }
        $node->setAttribute('title', $lookup[$key]);
    }

}
