<?php

/**
 * Extracts metadata from XHTMLCompiler_Page (which comes the version
 * control system) and inserts it in the HTML page.
 */
class XHTMLCompiler_DOMFilter_Metadata extends XHTMLCompiler_DOMFilter {

    protected $name = 'Metadata';

    public function process(DOMDocument $dom, $page, $manager) {

        $head = $dom->getElementsByTagName('head')->item(0);
        if (!$head) return;
        $manager->addDependency(__FILE__);

        $date = $page->getCreatedTime();
        // check if the meta element already exists
        $meta = $dom->createElement('meta');
        $append = true;
        foreach ($head->getElementsByTagName('meta') as $node) {
            if ($node->getAttribute('name') == 'Date') {
                // it exists: use this date as the "real" date.
                $meta = $node;
                if ($t = $meta->getAttribute('content')) {
                    // this code is slightly redundant since getCreatedTime()
                    // will have already accounted for this. Still, can't hurt.
                    $date = new DateTime($t);
                }
                $append = false;
                break;
            }
        }
        // only write if we have a valid date
        if ($date) {
            $meta->setAttribute('name', 'Date');
            $meta->setAttribute('content', $date->format('c'));
            if ($append) $head->appendChild($meta);
        }

    }

}
