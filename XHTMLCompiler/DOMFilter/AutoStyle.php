<?php

/**
 * Automatically adds external style-sheet inclusions based on file
 * naming conventions.
 * @todo Extend this to also glob CSS files into one file for better
 *       downloads
 */
class XHTMLCompiler_DOMFilter_AutoStyle extends XHTMLCompiler_DOMFilter
{

    protected $name = 'AutoStyle';

    public function process(DOMDocument $dom, $page, $manager) {

        $head = $this->query("//html:head")->item(0);
        $links = $this->query('link', $head);

        $manager->addDependency(__FILE__);

        $preloaded = array();
        $prefix = $page->getDirSName();
        foreach ($links as $link) {
            // ensure the link is importing CSS stylesheets
            if ($link->getAttribute('rel') != 'stylesheet') continue;
            if ($link->getAttribute('type') != 'text/css') continue;
            $path = $link->getAttribute('href');
            if (empty($path) || $path[0] === '/' || $path[0] === '.') {
                // do not attempt to manage special paths
                continue;
            }
            $preloaded[$prefix . $path] = true;
        }

        $dir = $page->getDir();
        $css_files = $dir->scanFlat('.css');
        $base = $page->getPathStem();

        foreach ($css_files as $file) {
            // determine whether or not it should be included
            // TODO: extend this algorithm to allow different media
            // types in form pagename.mediatype.css
            if (strpos($file, $base) === 0) {
                // if already included, don't re-include
                if (isset($preloaded[$file])) continue;
                $link = $dom->createElement('link');
                $link->setAttribute('rel', 'stylesheet');
                $link->setAttribute('type', 'text/css');
                // because scanFlat() prepends the directory name to
                // each of the files, in a web-context we need to remove
                // it
                $link->setAttribute('href', basename($file));
                $head->appendChild($link);
            }
        }

    }

}
