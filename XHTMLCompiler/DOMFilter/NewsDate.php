<?php

/**
 * This filter, closely related with the News filter, generates the
 * Date metadata associated with a news article.
 */
class XHTMLCompiler_DOMFilter_NewsDate extends XHTMLCompiler_DOMFilter_NewsBase
{
    protected $name = 'NewsDate';
    public function process(DOMDocument $dom, $page, $manager) {
        if (!isset($page->attr['news'])) return;
        $dateTime = $page->getCreatedTime();
        if (!$dateTime) return;
        $manager->addDependency(__FILE__);
        $content = $dom->getElementById('content');
        $body  = $content->parentNode;
        $date  = $this->makeDateNode($dom, $dateTime);
        $date->setAttribute('id', 'news-date');
        $body->insertBefore($date, $content);
    }
}
