<?php

/**
 * Generates an RSS feed from a specially designed HTML container indicated
 * by rss:for.
 */
class XHTMLCompiler_DOMFilter_RSSGenerator extends XHTMLCompiler_DOMFilter
{

    protected $name = 'RSSGenerator';
    protected $prefix = 'rss';

    // new namespace defines the following attributes:
    // for - IDREF to an element where we are to get the RSS info from

    public function process(DOMDocument $dom, $page, $manager) {

        // attempt to find declarations of the namespace
        $nodes = $this->query(
            "//attribute::*[namespace-uri() = '".$this->ns."']"
        );
        if (!$nodes->length) return;
        $manager->addDependency(__FILE__);

        // grab the document's links to RSS feeds
        // we require that the link have a href, a title and a type
        // as well as an rss:for attribute specifying where to grab data
        $links = $this->query('//html:link[@rss:for]');

        foreach ($links as $link) {
            $this->generateRSS($dom, $link, $page);
        }

    }

    /**
     * Generates the RSS feed for a specific link in a document
     * @param $link <link> DOMElement we're generating feed for
     * @param $page Page we're generating for
     */
    protected function generateRSS($dom, $link, $page) {

        // retrieve web-path of the page
        $path = $page->getWebPath();
        $xc = XHTMLCompiler::getInstance();

        // generate RSS template
        $rss = new XHTMLCompiler_RSSFeed(
            $link->getAttribute('title'),
            $path,
            $this->confiscateAttr($link, $this->ns, 'description'),
            $lang = $dom->documentElement->getAttribute('xml:lang')
        );

        // retrieve data source
        $id = $this->confiscateAttr($link, $this->ns, 'for');
        $data_source = $dom->getElementById($id);

        // parse data source, add news items
        foreach ($data_source->childNodes as $src_item) {
            if (! $src_item instanceof DOMElement) continue;
            if ($src_item->getAttribute('class') !== 'item') continue;

            $title = $date = $body = '';
            foreach ($src_item->childNodes as $element) {
                if (! $element instanceof DOMElement) continue;
                $var = $element->getAttribute('class');
                if ($var == 'date') {
                    if (($node = $element->childNodes->item(0)) instanceof DOMElement) {
                        // If we are using human readable dates but
                        // non-parseable machine dates, an abbr
                        // element should be inside the element with class="date"
                        // with a class atNNN where NNN is the Unix timestamp
                        // of publishing.
                        $datetime = new DateTime('@' . substr($node->getAttribute('class'), 2));
                        $date = $datetime->format('r');
                    } else {
                        $date = $element->textContent;
                    }
                } elseif ($var == 'permalink') {
                    $permalink = $element->childNodes->item(0)->getAttribute('href');
                } elseif ($var == 'title' || $var == 'body') {
                    $$var = $element->textContent;
                }
            }

            if (isset($permalink)) {
                // assume that the permalink is an absolute path
                if ($permalink[0] !== '/') {
                    trigger_error("Permalink for '$title' is not absolute");
                    continue;
                }
                $article_link = 'http://' . $xc->getConf('web_domain') . $permalink;
            } else {
                // determine the article link, based off anchors
                $item_id = $src_item->getAttribute('id');
                if (!$item_id) {
                    trigger_error("News item '$title' has no ID");
                    continue;
                }
                $article_link = $path . '#' . $item_id;
            }
            $rss->addItem($article_link, $title, $date, $body);

        }

        // save the feed
        $rss->save(
            $page->normalizePath(
                $link->getAttribute('href')
            )
        );

    }

}
