<?php

/**
 * Implements the xc:absolute attribute, which allows links to be made
 * relative to the XHTML Compiler configured document root (and not
 * the absolute web root.) This greatly simplifies inter-directory linking
 * and styling.
 */
class XHTMLCompiler_DOMFilter_AbsolutePath extends XHTMLCompiler_DOMFilter
{

    protected $name = 'AbsolutePath';
    protected $xcAttr = array('absolute');

    public function process(DOMDocument $dom, $page, $manager) {

        // this is the absolute path implementation of the functionality
        $xc = XHTMLCompiler::getInstance();
        $prefix = $xc->getConf('web_path') . '/';

        // this is the relative path implementation of the functionality
        //$prefix = str_repeat('../', $page->getDepth());

        $nodes = $this->query("//*[@xc:absolute]");
        if ($nodes) $manager->addDependency(__FILE__);

        foreach ($nodes as $node) {
            $attribute = $this->confiscateAttr($node, $this->ns, 'absolute');
            if ($attribute) {
                // $attribute is the name of the attribute to convert
                // to absolute form, we can extend the syntax to allow
                // multiple attributes.
                $uri = $node->getAttribute($attribute);
                // special cases
                if ($uri == '.' && $prefix) $uri = '';
                $node->setAttribute($attribute, $prefix . $uri);
            }
        }

    }

}
