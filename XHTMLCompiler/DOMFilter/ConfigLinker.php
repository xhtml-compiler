<?php

/**
 * Implements the xc:configdoc attribute on a tags, which gets converted
 * into a link to the appropriate configuration documentation, and,
 * if the contents of the tag is empty, expands the configuration
 * directive text to %NS.Directive.
 */
class XHTMLCompiler_DOMFilter_ConfigLinker extends XHTMLCompiler_DOMFilter
{

    protected $name = 'ConfigLinker';
    protected $xcAttr = array('configdoc');

    public function process(DOMDocument $dom, $page, $manager) {

        // this is the absolute path implementation of the functionality
        $xc = XHTMLCompiler::getInstance();
        $configdoc = $xc->getConf('configdoc');
        $nodes = $this->query("//html:a[@xc:configdoc]");
        if ($nodes) $manager->addDependency(__FILE__);
        foreach ($nodes as $node) {
            $directive = $this->confiscateAttr($node, $this->ns, 'configdoc');
            if (!$configdoc) continue;
            if ($directive) {
                // $directive is "Namespace.Directive" to convert
                $node->setAttribute('href', str_replace('%s', $directive, $configdoc));
                if (!$node->hasChildNodes()) {
                    $text = $dom->createTextNode('%' . $directive);
                    $node->appendChild($text);
                }
            }
        }

    }

}
