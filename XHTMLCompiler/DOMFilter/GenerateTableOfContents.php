<?php

/**
 * Generates a table of contents based on the heading structure of the
 * document.
 */
class XHTMLCompiler_DOMFilter_GenerateTableOfContents extends XHTMLCompiler_DOMFilter
{

    protected $name = 'GenerateTableOfContents';

    public function process(DOMDocument $dom, $page, $manager) {

        // test for ToC container, if not present don't bother
        // currently, only id="toc" is supported, which means there can
        // only be one table of contents per page
        $container = $this->query("//html:div[@id='toc']")->item(0);
        if (!$container) return;
        $manager->addDependency(__FILE__);

        // grab all headings h2 and down from the document
        $headings = array('h2', 'h3', 'h4', 'h5', 'h6');
        foreach ($headings as $k => $v) $headings[$k] = "self::html:$v";
        $query_headings = implode(' or ', $headings);
        $query = "//*[$query_headings]"; // looks like "//*[self::html:h2 or ...]"
        $headings = $this->query($query);

        // setup the table of contents element
        $toc = $dom->createElement('ul');
        $toc->setAttribute('class', 'toc-base');
        $container->appendChild($dom->createElement('h2', 'Table of Contents'));
        $container->appendChild($toc);

        // iterate through headings and build the table of contents
        $current_level = 2;
        $parents = array(false, $toc);
        $indexes = array(0);
        $i = 0;
        foreach ($headings as $node) {
            $level = (int) $node->tagName[1];
            $name  = $node->textContent; // no support for formatting

            while ($level > $current_level) {
                if (!$parents[$current_level-1]->lastChild) {
                    $parents[$current_level-1]->appendChild(
                        $dom->createElement('li')
                    );
                }
                $sublist = $dom->createElement('ul');
                $parents[$current_level - 1]->lastChild->appendChild($sublist);
                $parents[$current_level] = $sublist;
                $current_level++;
                $indexes[$current_level - 2] = 0;
            }

            while ($level < $current_level) {
                unset($indexes[$current_level - 2]);
                $current_level--;
            }

            $indexes[$current_level - 2]++;


            $line = $dom->createElement('li');
            $label = $dom->createElement('span', implode('.', $indexes) . '.');
            $label->setAttribute('class', 'toc-label');
            $line->appendChild($label);
            $link = $dom->createElement('a', htmlspecialchars($name));
            $line->appendChild($link);
            $parents[$current_level-1]->appendChild($line);

            // setup the anchors
            $header_id = $node->getAttribute('id');
            if (!$header_id) {
                // automatically generate a header. These are volatile,
                // so it's highly recommended that you manually specify
                // them
                // TODO: generate based on the contents of the header node
                $header_id = 'toclink' . $i;
                $node->setAttribute('id', $header_id);
                $i++;
            }
            $link->setAttribute('href', '#' . $header_id);

        }
    }

}
