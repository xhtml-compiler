<?php

/**
 * Mini blog-style filter for globbing contents of another directory
 * and inserting summaries into a main page.
 * @note This filter should be run before other "cosmetic" filters, as
 *       it makes major changes to page content and may pull data from
 *       sources that have not been processed yet.
 */
class XHTMLCompiler_DOMFilter_News extends XHTMLCompiler_DOMFilter_NewsBase
{
    protected $name = 'News';
    protected $prefix = 'news';
    protected $xcAttr = array('news');
    public function process(DOMDocument $dom, $page, $manager) {
        // Remove the semaphore xc:news attribute, and assign to $page
        // appropriately
        $dep = false;
        if ($this->confiscateAttr($dom->documentElement, 'xc', 'news')) {
            $dep = true;
            $page->attr['news'] = true;
        }

        $containers = $this->query("//html:div[@news:source]");
        foreach ($containers as $container) {
            $dep = true;
            $this->generateNews($container, $dom, $page, $manager);
        }
        if ($dep) $manager->addDependency(__FILE__);

    }

    /**
     * Generates a particular news container.
     */
    public function generateNews($container, DOMDocument $dom, $page, $manager) {

        // Grab variables
        $source = $this->confiscateAttr($container, $this->ns, 'source');
        $source = $page->normalizePath($source);
        $limit = $this->confiscateAttr($container, $this->ns, 'limit');
        if (!$limit) $limit = 5;
        $header = $this->confiscateAttr($container, $this->ns, 'header');
        if (!$header) $header = 'h2';
        $idPrefix = $this->confiscateAttr($container, $this->ns, 'idPrefix');
        if (!$idPrefix) $idPrefix = '';
        $increase = $header[1] - 1; // amount increase sub-headings

        // Recursively glob for source files
        // :TODO: add DI for this
        $fs = new FSTools();
        $result = $fs->globr($source, '*.xhtml');
        // This doesn't work if there is more than one entry in a day
        rsort($result);

        // Setup manager
        $xc = XHTMLCompiler::getInstance();
        $manager = $xc->getFilterManager();

        for ($i = $d = 0, $c = count($result); $i < $c && $d != $limit; $i++) {
            $entry = $result[$i];

            // :TODO: Add support for nested directories. Also need to modify
            // generateId when that happens.
            $base = basename($entry);
            if (strlen($base) < 4 || !ctype_digit(substr($base, 0, 4))) {
                continue;
            }

            $entryFile = new XHTMLCompiler_Page($entry);
            // This DOM has IDs setup, but no other processing setup.
            $subdom = $manager->parse($entryFile->getSource(), $entryFile);

            $entryNode = $dom->createElement('div');
            $entryNode->setAttribute('class', 'item');

            // Generate ID for this entry. The format is
            // year-month-date-lc-title-with-dashes
            $entryNode->setAttribute('id', $idPrefix . $this->generateId($entryFile->getPathStem()));

            // Grab the title
            $node = $subdom->getElementsByTagName('h1')->item(0);
            $h1 = $dom->importNode($node, true);
            $hx = $dom->createElement($header);
            $hx->setAttribute('class', 'title');
            foreach ($h1->childNodes as $h1node) $hx->appendChild($h1node);
            $entryNode->appendChild($hx);

            // Possible place for factoring out. Also, we can also add
            // a timezone to be more like ours.
            $dateTime = $entryFile->getCreatedTime();
            if ($dateTime) {
                $entryNode->appendChild($this->makeDateNode($dom, $dateTime));
            }

            // Grab the content
            // :WARNING: This code apparently leaves behind an
            // xmlns statement, although we're not quite sure why.
            $node = $subdom->getElementById('short-content');
            $permText = 'Read more...';
            if (!$node) {
                $node = $subdom->getElementById('content');
                $permText = 'Permalink';
            }
            $node = $dom->importNode($node, true);
            $this->confiscateAttr($node, 'id');
            $node->setAttribute('class', 'body');
            $entryNode->appendChild($node);

            // Make permalink
            $permalink = $dom->createElement('div');
            $permalink->setAttribute('class', 'permalink');
            $a = $dom->createElement('a');
            $a->setAttribute('href', $entryFile->getAbsolutePath());
            $a->appendChild($dom->createTextNode($permText));
            $permalink->appendChild($a);
            $entryNode->appendChild($permalink);

            $container->appendChild($entryNode);

            $manager->addDependency($entry);

            // increment one successful
            $d++;
        }
    }

    /**
     * Generates an ID based on the filename of a blog entry
     */
    public function generateId($entry) {
        $parts = array_slice(explode('/', $entry), -2);
        // A very specific format: year/monthday-entry-name, like 2008/0131-foobar
        // Arbitrary folders before that is ok.
        $entry = 'entry-' . $parts[0] .
            '-' . substr($parts[1], 0, 2) .
            '-' . substr($parts[1], 2, 2) .
            '-' . substr($parts[1], 5);
        return $entry;
    }

}
