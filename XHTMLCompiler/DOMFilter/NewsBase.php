<?php

/**
 * Base class for news related functionality, does common functions.
 */
abstract class XHTMLCompiler_DOMFilter_NewsBase extends XHTMLCompiler_DOMFilter
{
    protected function makeDateNode($dom, $dateTime) {
        $time = $dateTime->format('g:i A T');
        $date = $dateTime->format('l, F j, Y');
        $dateNode = $dom->createElement('div');
        $dateNode->setAttribute('class', 'date');
        $abbrNode = $dom->createElement('abbr');
        $abbrNode->setAttribute('class', 'at' . $dateTime->format('U'));
        $dateText = $dom->createTextNode("Posted $time on $date");
        $abbrNode->appendChild($dateText);
        $dateNode->appendChild($abbrNode);
        return $dateNode;
    }
}

