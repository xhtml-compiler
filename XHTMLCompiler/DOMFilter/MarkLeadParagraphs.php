<?php

/**
 * Attaches class="lead" to all leading paragraphs for typography.
 */
class XHTMLCompiler_DOMFilter_MarkLeadParagraphs extends XHTMLCompiler_DOMFilter
{

    protected $name = 'MarkLeadParagraphs';
    protected $className = 'lead';

    public function __construct($class = false, $name = false) {
        parent::__construct($name);
        if ($class) $this->className = $class;
    }

    public function process(DOMDocument $dom, $page, $manager) {
        // possibly add some attribute which disables this behavior
        $nodes = $this->query("//html:p[local-name(preceding-sibling::*[1])!='p']");
        if ($nodes) $manager->addDependency(__FILE__);
        foreach ($nodes as $node) {
            $node->setAttribute('class', $this->className);
        }
    }

}
