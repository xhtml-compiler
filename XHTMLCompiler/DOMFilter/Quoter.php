<?php

/**
 * Converts <q> tags into quotation marks.
 */
class XHTMLCompiler_DOMFilter_Quoter extends XHTMLCompiler_DOMFilter
{

    protected $name = 'Quoter';

    protected $leftQuote = "\xE2\x80\x9C";
    protected $rightQuote = "\xE2\x80\x9D";

    protected $leftNestedQuote = "\xE2\x80\x98";
    protected $rightNestedQuote = "\xE2\x80\x99";

    public function __construct(
        $left = null, $right = null,
        $left_nest = null, $right_nest = null,
        $name = false
    ) {
        parent::__construct($name);
        if ($left)          $this->leftQuote = $left;
        if ($right)         $this->rightQuote = $right;
        if ($left_nest)     $this->leftNestedQuote = $left_nest;
        if ($right_nest)    $this->rightNestedQuote = $right_nest;
    }

    public function process(DOMDocument $dom, $page, $manager) {

        $dep = false;
        // first handle single-quotes
        $nodes = $this->query("//html:q//html:q");
        foreach ($nodes as $node) {
            $dep = true;
            $this->quotify($dom, $node, $this->leftNestedQuote, $this->rightNestedQuote);
        }

        // now handle double-quotes
        $nodes = $this->query("//html:q");
        foreach ($nodes as $node) {
            $dep = true;
            $this->quotify($dom, $node, $this->leftQuote, $this->rightQuote);
        }
        if ($dep) $manager->addDependency(__FILE__);

    }

    /**
     * Takes a node and unpacks its contents, surrounding it with
     * text instead.
     * @param $node Node to be unpacked
     * @param $left New text wrapper on left
     * @param $right New text wrapper on right
     */
    protected function quotify($dom, $node, $left, $right) {
        $parent = $node->parentNode;
        $parent->insertBefore($dom->createTextNode($left), $node);
        foreach ($node->childNodes as $child) {
            $parent->insertBefore($child->cloneNode(true), $node);
        }
        $parent->insertBefore($dom->createTextNode($right), $node);
        $parent->removeChild($node);
    }

}
