<?php

/**
 * These classes are responsible for converting a text in a markup
 * specific language to the corresponding HTML code. This process can
 * be somewhat intense, as it may be necessary to find the template
 * to insert the markup; furthermore, the markup generator being used
 * is not guaranteed to generate conformant markup.
 */
abstract class XHTMLCompiler_Markup extends XHTMLCompiler_Filter
{

}

