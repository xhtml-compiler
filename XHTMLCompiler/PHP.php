<?php

/**
 * This class serves as a wrapper to native PHP functions that have
 * external effects or influences; we are consequently able to stub
 * this class for unit testing.
 * @note Please retrieve this class using XHTMLCompiler::getPHPWrapper()
 * @note PHP's original function names were normalized to use camel-caps
 * @todo Factor out filesystem related functionality to another wrapper?
 */
class XHTMLCompiler_PHP
{

    /** Sends an HTTP header, equivalent to header() */
    public function header() {
        $args = func_get_args();
        call_user_func_array('header', $args);
    }

    /** Sends HTTP headers to indicate output is XML **/
    public function headerXML() {
        $this->header('Content-type: application/xml');
    }

    /** Outputs text, equivalent to echo */
    public function paint($text) { echo $text; }

    /** Retrieves the relative URI with which the page was requested. */
    public function getRequestURI() {return $_SERVER['REQUEST_URI'];}

    /** Retrieves the relative URI which denotes the frontend PHP file */
    public function getPHPSelf() {return $_SERVER['PHP_SELF'];}

    /** Retrieves a parameter from GET superglobal, does magic quote cleaning */
    public function getGVal($key) {
        if (!isset($_GET[$key])) return false;
        $val = $_GET[$key];
        // ad hoc magic_quotes protection, not comprehensive because
        // we don't deal with very complicated parameters.
        if (is_string($val) && get_magic_quotes_gpc()) $val = stripslashes($val);
        return $val;
    }

    /** Returns true if passed filename is directory */
    public function isDir($dir) {return is_dir($dir);}

    /** Returns true if passed filename is file */
    public function isFile($file) {return is_file($file);}

    /**
     * Resolves a relative path into an absolute one
     * @note This also normalizes Windows backslashes into forward slashes
     */
    public function realpath($path) {
        $path = realpath($path);
        if ($path === false) return false;
        return str_replace('\\', '/', $path);
    }

    /** Returns HTTP status code server was originally going to send */
    public function getRedirectStatus() {return (int) getenv("REDIRECT_STATUS");}

}
