<?php

/**
 * Converts documents that have the xc-fragment class on the body
 * element into HTML fragments, which are only the contents of the
 * body element
 */
class XHTMLCompiler_TextFilter_Fragment extends XHTMLCompiler_TextFilter
{

    var $name = 'Fragment';

    public function process($text, $page, $manager) {
        if (strpos($text, '<body class="xc-fragment">') === false) return $text;
        preg_match('/<body class="xc-fragment">(.+?)<\/body>/s', $text, $matches);
        file_put_contents($page->getPathStem() . '.frag', $matches[1]);
        return $text;
    }

}
