<?php

/**
 * Represents a file in the filesystem
 */
class XHTMLCompiler_File
{

    /** Filename of file this object represents */
    protected $name;

    /**
     * Filename of file you wish to instantiate.
     * @note This file need not exist
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * Returns the filename of the file.
     */
    public function getName() {return $this->name;}

    /**
     * Returns directory of the file without trailing slash
     */
    public function getDirectory() {return dirname($this->name);}

    /**
     * Retrieves the contents of a file
     * @todo Throw an exception if file doesn't exist
     */
    public function get() {
        return file_get_contents($this->name);
    }

    /**
     * Writes contents to a file, creates new file if necessary
     * @param $contents String contents to write to file
     */
    public function write($contents) {
        file_put_contents($this->name, $contents);
    }

    /**
     * Deletes the file
     */
    public function delete() {
        unlink($this->name);
    }

    /**
     * Returns true if file exists and is a file.
     */
    public function exists() {
        $php = XHTMLCompiler::getPHPWrapper();
        return $php->isFile($this->name);
    }

    /**
     * Returns last file modification time
     */
    public function getMTime() {
        return filemtime($this->name);
    }

    /** Returns the last inode modifiation time */
    public function getCTime() {
        return filectime($this->name);
    }

    /**
     * Chmod a file
     * @note We ignore errors because of some weird owner trickery due
     *       to SVN duality
     */
    public function chmod($octal_code) {
        @chmod($this->name, $octal_code);
    }

    /**
     * Touches a file.
     */
    public function touch() {
        touch($this->name);
    }
}
