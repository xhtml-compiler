<?php

class XHTMLCompiler_Markup_Markdown extends XHTMLCompiler_TextFilter
{
    
    var $name = 'Markdown';
    
    public function process($text, $page, $manager) {
        // This code assumes that the input is \n. I don't the code actually
        // handles this right now.
        $title = "Untitled";
        $bits = explode("\n", $text, 3);
        if (isset($bits[2])) {
            if (trim($bits[0]) === '---') {
                // this means we've got YAML: find the next --- line
                $end = strpos($text, "---\n", 4);
                $yaml = substr($text, 4, $end - 4);
                $data = Spyc::YAMLLoad($yaml);
                $title = $data['title'];
                $text = substr($text, $end + 4);
            } elseif (strlen($bits[1]) > 0 && trim($bits[1], '=') === '') {
                // parse out a leading header, which we will treat as the header
                // (this is the only bit of metadata we're going to do)
                $text = $bits[2];
                $title = $bits[0];
            }
        }

        $body = Markdown($text);
        // perform template search
        $dir = $page->getDir();
        $template = $dir->getFile('template.xml');
        while (!$template->exists()) {
            $dir = $dir->getParent();
            if (!$dir->isAllowed()) break;
            $template = $dir->getFile('template.xml');
        }
        if (!$template->exists()) {
            // maybe a cheap copout, maybe an exception
            throw new XHTMLCompiler_Exception(500, 'No template file',
                'Markup snippet had no template file to inline into.');
        }
        $html = $template->get();
        $html = str_replace('{body}', $body, $html);
        $html = str_replace('{title}', $title, $html);
        return $html;
    }
    
}
