<?php

class XHTMLCompiler_FunctionsTest extends XHTMLCompilerHarness
{

    function test_set_response_code() {
        $this->php->expectAt(0, 'header', array($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404));
        $this->php->expectAt(1, 'header', array('Status: 404 Not Found'));
        set_response_code(404);
    }

    function test_exception_handler() {
        $this->xc->setReturnValue('getConf', XHTMLCOMPILER . '/error.xsl', array('error_xsl'));
        $this->php->expect('paint', array(new PatternExpectation('/404 Not Found/')));
        xhtmlcompiler_exception_handler(new XHTMLCompiler_Exception(404));
        $this->php->expect('paint', array(new PatternExpectation('/WTF/')));
        xhtmlcompiler_exception_handler(new XHTMLCompiler_Exception(404, 'WTF'));
        $this->php->expect('paint', array(new PatternExpectation('/Could not find the file/')));
        xhtmlcompiler_exception_handler(new XHTMLCompiler_Exception(404, false, 'Could not find the file'));
    }

    function test_get_page_from_server() {
        $this->php->setReturnValue('getRequestURI', '/index.html');
        $this->php->setReturnValue('getPHPSelf', '/xhtml-compiler/main.php');
        $this->assertEqual(get_page_from_server(), 'index.html');
    }

    function test_get_page_from_server_InSubdir() {
        $this->php->setReturnValue('getRequestURI', '/subdir/foobar.html');
        $this->php->setReturnValue('getPHPSelf', '/subdir/xhtml-compiler/main.php');
        $this->assertEqual(get_page_from_server(), 'foobar.html');
    }

    function test_get_page_from_server_InSubdirDifferentAppDir() {
        $this->php->setReturnValue('getRequestURI', '/subdir/foobar.html');
        $this->php->setReturnValue('getPHPSelf', '/subdir/xc/main.php');
        $this->assertEqual(get_page_from_server(), 'foobar.html');
    }

    function test_get_page_from_server_InSubdirPageInDirectory() {
        $this->php->setReturnValue('getRequestURI', '/subdir/foo/foobar.html');
        $this->php->setReturnValue('getPHPSelf', '/subdir/xhtml-compiler/main.php');
        $this->assertEqual(get_page_from_server(), 'foo/foobar.html');
    }

    function test_normalize_index_Blank() {
        $this->assertEqual(normalize_index('', 'index.html'), 'index.html');
    }

    function test_normalize_index_File() {
        $this->php->expectOnce('isDir', array('main.html'));
        $this->php->setReturnValue('isDir', false);
        $this->assertEqual(normalize_index('main.html', 'index.html'), 'main.html');
    }

    function test_normalize_index_LooksLikeFileButIsDir() {
        $this->php->expectOnce('isDir', array('main.html'));
        $this->php->setReturnValue('isDir', true);
        $this->assertEqual(normalize_index('main.html', 'index.html'), 'main.html/index.html');
    }

    function test_normalize_index_SubDir() {
        $this->php->expectOnce('isDir', array('foo/'));
        $this->php->setReturnValue('isDir', true);
        $this->assertEqual(normalize_index('foo/', 'index.html'), 'foo/index.html');
    }

    function test_normalize_index_SubDirMissingSlash() {
        $this->php->expectOnce('isDir', array('foo'));
        $this->php->setReturnValue('isDir', true);
        $this->assertEqual(normalize_index('foo', 'index.html'), 'foo/index.html');
    }

    function test_normalize_index_FileWithoutExtension() {
        $this->php->expectOnce('isDir', array('foo'));
        $this->php->setReturnValue('isDir', false);
        $this->assertEqual(normalize_index('foo', 'index.html'), 'foo');
    }

}

?>
