<?php

/**
 * Test harness that sets up a filesystem sandbox for file-emulation
 * functions to safely unit test in.
 */
class XHTMLCompiler_FileSystemHarness extends UnitTestCase
{
    
    protected $dir, $oldDir;
    
    public function __construct() {
        parent::__construct();
        $this->dir = 'tmp/' . md5(uniqid(rand(), true)) . '/';
        mkdir($this->dir);
        $this->oldDir = getcwd();
        
    }
    
    public function __destruct() {
        // individual tests must be sure to destroy their own files!
        rmdir($this->dir);
    }
    
    public function setup() {
        chdir($this->dir);
    }
    
    public function tearDown() {
        chdir($this->oldDir);
    }
    
}
