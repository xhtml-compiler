<?php

class XHTMLCompiler_ExceptionTest extends UnitTestCase
{
    function test() {
        $e = new XHTMLCompiler_Exception(500, 'Argh!', 'Matey overboard'); $l = __LINE__;
        $this->assertEqual($e->getCode(), 500);
        $this->assertEqual($e->getMessage(), 'Argh!');
        $this->assertEqual($e->getDetails(), 'Matey overboard');
        $this->assertEqual($e->getLine(), $l); // test Exception inheritance
    }
}

?>