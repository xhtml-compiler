<?php

class XHTMLCompiler_FileTest extends XHTMLCompiler_FileSystemHarness
{
    
    function test() {
        $name = 'test.txt';
        $file = new XHTMLCompiler_File($name);
        $this->assertFalse($file->exists());
        $file->write('foobar');
        $this->assertTrue($file->exists());
        $this->assertEqual($file->get(), 'foobar');
        $file->delete();
        $this->assertFalse($file->exists());
    }
    
    function testGetNonExistent() {
        $name = 'notfound.txt';
        $this->expectError();
        $file = new XHTMLCompiler_File($name);
        $this->assertFalse($file->get());
    }
    
}

