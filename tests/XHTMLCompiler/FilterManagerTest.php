<?php

class XHTMLCompiler_FilterManagerTest_DOMFilter extends XHTMLCompiler_DOMFilter
{
    protected $name = 'Test';
    function process(DOMDocument $dom, $page, $manager) {
        $dom->getElementById('document')->appendChild($dom->createElement('p', 'DOMFilter'));
    }
}

class XHTMLCompiler_FilterManagerTest extends UnitTestCase
{
    
    function test_addFilter() {
        $manager = new XHTMLCompiler_FilterManager();
        $filter = new XHTMLCompiler_DOMFilterMock($this);
        $filter->expectOnce('getName');
        $filter->setReturnValue('getName', 'filter');
        $filter->setReturnValue('getXCAttributesDefined', array());
        $manager->addDOMFilter($filter);
    }
    
    function test_addFilter_Duplicate() {
        $manager = new XHTMLCompiler_FilterManager();
        $filter = new XHTMLCompiler_DOMFilterMock($this);
        $filter->setReturnValue('getName', 'filter');
        $filter->setReturnValue('getXCAttributesDefined', array());
        $filter2 = new XHTMLCompiler_DOMFilterMock($this);
        $filter2->setReturnValue('getName', 'filter');
        $manager->addDOMFilter($filter);
        $this->expectException();
        $manager->addDOMFilter($filter2);
    }
    
    function test_process() {
        $manager = new XHTMLCompiler_FilterManager();
        $page = new XHTMLCompiler_PageMock();
        $input = '1';
        
        // using new IsAExpectation('XHTMLCompiler_FilterManager')
        // since SimpleTest chokes on recursive dependencies
        
        $pre_text_filter = new XHTMLCompiler_TextFilterMock($this);
        $pre_text_filter->setReturnValue('getName', 'banana-filter');
        $pre_text_filter->expectOnce('process', array('1', $page, new IsAExpectation('XHTMLCompiler_FilterManager')));
        $pre_text_filter->setReturnValue('process', '2');
        $pre_text_filter2 = new XHTMLCompiler_TextFilterMock($this);
        $pre_text_filter2->setReturnValue('getName', 'apple-filter');
        $pre_text_filter2->expectOnce('process', array('2', $page, new IsAExpectation('XHTMLCompiler_FilterManager')));
        $pre_text_filter2->setReturnValue('process',
'<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
xmlns:proprietary="urn:foobar:foofoofoo">
<head><title>Hmmm...</title></head>
<body id="document"><p>Meta.</p></body>
</html>
        ');
        
        // the first two DOMFilters are duds that don't do anything
        $dom_filter = new XHTMLCompiler_DOMFilterMock($this);
        $dom_filter->setReturnValue('getName', 'filter');
        $dom_filter->setReturnValue('getXCAttributesDefined', array());
        $dom_filter->expectOnce('process');
        $dom_filter2 = new XHTMLCompiler_DOMFilterMock($this);
        $dom_filter2->setReturnValue('getName', 'filter2');
        $dom_filter2->setReturnValue('getXCAttributesDefined', array());
        $dom_filter2->expectOnce('process');
        $dom_filter3 = new XHTMLCompiler_FilterManagerTest_DOMFilter();
        
        $post_text_filter = new XHTMLCompiler_TextFilterMock($this);
        $post_text_filter->setReturnValue('getName', 'Masumune Plus!');
        $post_text_filter->expectOnce('process', array(new PatternExpectation('/Meta.+?DOMFilter/s'), $page, new IsAExpectation('XHTMLCompiler_FilterManager')));
        $post_text_filter->setReturnValue('process', '3');
        $post_text_filter2 = new XHTMLCompiler_TextFilterMock($this);
        $post_text_filter2->setReturnValue('getName', 'Piffle-rockers');
        $post_text_filter2->expectOnce('process', array('3', $page, new IsAExpectation('XHTMLCompiler_FilterManager')));
        $post_text_filter2->setReturnValue('process',
'<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><title>Hmmm...</title></head>
<body><b><p>Hotdog.</p></b></body>
</html>
        ');
        
        $manager->addPreTextFilter($pre_text_filter);
        $manager->addPreTextFilter($pre_text_filter2);
        $manager->addDOMFilter($dom_filter);
        $manager->addDOMFilter($dom_filter2);
        $manager->addDOMFilter($dom_filter3);
        $manager->addPostTextFilter($post_text_filter);
        $manager->addPostTextFilter($post_text_filter2);
        
        $text = $manager->process('1', $page);
        
        // assert that the final PostTextFilter worked
        $this->assertPattern('/Hotdog/', $text);
        
        // assert that the validation errors were caught and
        // displayed accordingly
        $this->assertPattern('/Error/i', $text);
        
        // assert that all proprietary namespaces were scrubbed
        $this->assertNoPattern('/proprietary/', $text);
    }
    
}

?>