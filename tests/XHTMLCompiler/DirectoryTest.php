<?php

class XHTMLCompiler_DirectoryTest extends XHTMLCompiler_FileSystemHarness
{
    
    function testSlashNormalization() {
        mkdir('foo');
        $dir = new XHTMLCompiler_Directory('foo');
        $this->assertIdentical($dir->getName(), 'foo');
        $dir = new XHTMLCompiler_Directory('foo/');
        $this->assertIdentical($dir->getName(), 'foo');
        $dir = new XHTMLCompiler_Directory('foo\\');
        $this->assertIdentical($dir->getName(), 'foo');
        rmdir('foo');
    }
    
    function testNonexistent() {
        $this->expectException();
        $dir = new XHTMLCompiler_Directory('null');
    }
    
    function test_getTree() {
        mkdir('1');  mkdir('1/2'); mkdir('1/4');
        file_put_contents('1/2/3.txt', 'foobar');
        $dir = new XHTMLCompiler_Directory('');
        $tree = $dir->getTree();
        $this->assertEqual($tree, array(
            '1' => array(
                '2' => array(
                    '3.txt' => 6,
                ),
                '4' => array(),
            ),
        ));
        unlink('1/2/3.txt');
        rmdir('1/4'); rmdir('1/2'); rmdir('1');
    }
    
    function test_scan() {
        $dir = new XHTMLCompiler_Directory('');
        
        file_put_contents('1.txt', '1');
        file_put_contents('2.txt', '2');
        file_put_contents('3.txt', '3');
        $list = array('1.txt', '2.txt', '3.txt');
        $this->assertEqual($dir->scanRecursively('.txt'), $list);
        $this->assertEqual($dir->scanFlat('.txt'), $list);
        
        mkdir('foo');
        file_put_contents('foo/1.txt', '1');
        $this->assertEqual($dir->scanRecursively('.txt'), $a = array(
            '1.txt', '2.txt', '3.txt', 'foo/1.txt'
        ));
        $this->assertEqual($dir->scanFlat('.txt'), $list);
        
        file_put_contents('foo/1.log', '1'); // not matched
        $this->assertEqual($dir->scanRecursively('.txt'), $a);
        $this->assertEqual($dir->scan('.txt', 1), $a);
        $this->assertEqual($dir->scanFlat('.txt'), $list);
        $this->assertEqual($dir->scan('.txt', 0), $list);
        
        $dir = new XHTMLCompiler_Directory('foo');
        $this->assertEqual($dir->scan('.txt', true), array('foo/1.txt'));
        
        // cleanup
        unlink('foo/1.log'); unlink('foo/1.txt.');
        rmdir('foo');
        unlink('1.txt'); unlink('2.txt'); unlink('3.txt');
    }
    
}

?>