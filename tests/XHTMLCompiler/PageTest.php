<?php

class XHTMLCompiler_PageTest extends XHTMLCompilerHarness
{
    
    function setUp() {
        parent::setUp();
        // this is our "virtual filesystem", pay close attention!
        $this->xc->setReturnValue('getConf',
            array(
                '' => 0,
                'subdir' => 1, // recursion allowed
                'flatdir' => 0, // recursion not allowed
            ),
            array('allowed_dirs')
        );
        $this->php->setReturnValue('realpath', '/home/user', array('.'));
        $this->php->setReturnValue('realpath', '/home/user', array('./'));
        $this->php->setReturnValue('realpath', '/home/user', array(''));
        $this->php->setReturnValue('realpath', '/home/user/subdir', array('subdir'));
        $this->php->setReturnValue('realpath', '/home/user/flatdir', array('flatdir'));
    }
    
    function testConstruct() {
        $this->php->setReturnValue('isFile', true, array('index.xhtml'));
        $this->php->setReturnValue('isDir', true, array('.'));
        $this->php->setReturnValue('realpath', '/home/user/index.html', array('index.html'));
        $page = new XHTMLCompiler_Page('index.html');
        $this->assertEqual($page->getPathStem(), 'index');
    }
    
    function testConstructUnusualFilename() {
        $this->php->setReturnValue('isFile', true, array('@.xhtml'));
        $this->php->setReturnValue('isDir', true, array('.'));
        $page = new XHTMLCompiler_Page('@.html');
        $this->assertEqual($page->getPathStem(), '@');
    }
    
    function testConstructUnusualDirectory() {
        $this->php->setReturnValue('realpath', '/home/user/subdir/@', array('subdir/@'));
        $this->php->setReturnValue('isFile', true, array('subdir/@/index.xhtml'));
        $this->php->setReturnValue('isDir', true, array('subdir/@'));
        $page = new XHTMLCompiler_Page('subdir/@/index.html');
        $this->assertEqual($page->getPathStem(), 'subdir/@/index');
    }
    
    function testConstructInRecursiveDirectory() {
        $this->php->setReturnValue('realpath', '/home/user/subdir/foo', array('subdir/foo'));
        $this->php->setReturnValue('isFile', true, array('subdir/foo/index.xhtml'));
        $this->php->setReturnValue('isDir', true, array('subdir/foo'));
        $page = new XHTMLCompiler_Page('subdir/foo/index.html');
        $this->assertEqual($page->getPathStem(), 'subdir/foo/index');
    }
    
    function testBadConstructDueToExtension() {
        $this->expectException();
        $page = new XHTMLCompiler_Page('main.php');
    }
    
    function testBadConstructDueToBadDirectory() {
        $this->expectException();
        $this->php->setReturnValue('realpath', '/home/user/notallowed', array('notallowed'));
        $page = new XHTMLCompiler_Page('notallowed/main.html');
    }
    
    function testBadConstructDueToImaginaryDirectory() {
        $this->expectException();
        $this->php->setReturnValue('realpath', false, array('notallowed'));
        $page = new XHTMLCompiler_Page('notallowed/main.html');
    }
    
    function testBadConstructDueToImaginaryDirectoryInRecursiveDirectory() {
        $this->expectException();
        $this->php->setReturnValue('realpath', false, array('subdir/null'));
        $page = new XHTMLCompiler_Page('subdir/null/main.html');
    }
    
    function testBadConstructDueToNonRecursiveDirectory() {
        $this->expectException();
        $this->php->setReturnValue('realpath', '/home/user/flatdir/foobar', array('flatdir/foobar'));
        $page = new XHTMLCompiler_Page('flatdir/foobar/main.html');
    }
    
    function testBadConstructDueToNoSrcFile() {
        $this->expectException();
        $this->php->setReturnValue('isFile', false, array('index.xhtml'));
        $page = new XHTMLCompiler_Page('index.html');
    }
    
    function testBadConstructSharedPrefixWithGoodFolder() {
        $this->expectException();
        $this->php->setReturnValue('isFile', true, array('subdirectory/index.xhtml'));
        $this->php->setReturnValue('realpath', '/home/user/subdirectory', array('subdirectory'));
        $page = new XHTMLCompiler_Page('subdirectory/index.html');
    }
    
}

?>