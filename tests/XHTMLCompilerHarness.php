<?php

class XHTMLCompilerHarness extends UnitTestCase
{
    
    /**
     * Instances of the XHTMLCompiler and XHTMLCompiler_PHP mocks we
     * can configure.
     */
    protected $php, $xc;
    
    /**
     * The old XHTMLCompiler and XHTMLCompiler_PHP instances that are
     * restored after testing.
     */
    protected $oldPhp, $oldXc;
    
    function setUp() {
        $this->xc  = new XHTMLCompilerMock();
        $this->php = new XHTMLCompiler_PHPMock();
        if (empty($this->oldPhp) && empty($this->oldXc)) {
            $this->oldXc  = XHTMLCompiler::getInstance();
            $this->oldPhp = XHTMLCompiler::getPHPWrapper();
        }
        XHTMLCompiler::setInstance($this->xc);
        XHTMLCompiler::setPHPWrapper($this->php);
    }
    
    function tearDown() {
        XHTMLCompiler::setInstance($this->oldXc);
        XHTMLCompiler::setPHPWrapper($this->oldPhp);
    }
    
}

?>