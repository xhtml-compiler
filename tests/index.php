<?php

// call one file using /?f=FileTest.php , see $test_files array for
// valid values

error_reporting(E_ALL | E_STRICT);
define('XHTMLCompilerTest', 1);

$simpletest_location = 'simpletest/'; // reasonable guess

// load SimpleTest
if(file_exists($f = '../conf/config-test.php')) include $f;
if(file_exists($f = '../config-test.php')) include $f;
require_once $simpletest_location . 'unit_tester.php';
require_once $simpletest_location . 'reporter.php';
require_once $simpletest_location . 'mock_objects.php';

// note we are using a specially modded version of SimpleTest with strict
// support as well as a backported fix in exceptions.php that
// ensures tearDown gets called

// load our libraries
require_once '../common.php';
chdir(dirname(__FILE__));
Mock::generate('XHTMLCompiler', 'XHTMLCompilerMock');
Mock::generate('XHTMLCompiler_PHP', 'XHTMLCompiler_PHPMock');
Mock::generate('XHTMLCompiler_DOMFilter', 'XHTMLCompiler_DOMFilterMock');
Mock::generate('XHTMLCompiler_TextFilter', 'XHTMLCompiler_TextFilterMock');
Mock::generate('XHTMLCompiler_Page', 'XHTMLCompiler_PageMock');

require_once 'XHTMLCompilerHarness.php';
require_once 'XHTMLCompiler/FileSystemHarness.php';

$test = new TestSuite('All Tests - XHTML Compiler');

$test->addFile('XHTMLCompiler/FunctionsTest.php');
$test->addFile('XHTMLCompiler/PageTest.php');
$test->addFile('XHTMLCompiler/ExceptionTest.php');
$test->addFile('XHTMLCompiler/FilterManagerTest.php');
$test->addFile('XHTMLCompiler/DirectoryTest.php');

//if (isset($_GET['full'])) {
    // these are expensive unit tests
    $test->addFile('XHTMLCompiler/FileTest.php');
//}

if (SimpleReporter::inCli()) $reporter = new TextReporter();
else $reporter = new HTMLReporter('UTF-8');

$test->run($reporter);

